El propósito de este repositorio es para tener de una manera organizada el código fuente de la aplicación de recursos humanos "EvoHR"

### Estructura del Proyecto (Esquema de Carpetas) ###

EvoHR

--->Recursos
-> CSS
-> Fuentes
-> Img
-> JS
   -General
   -Node
   -Angular

---> Vista
-> V.Usuarios
-> V.Comun
   -encabezado.html
   -piePagina.html
   -menu.html
   -inicio.html

---> Controladores
-> C.Vista
   -cv.Usuario
-> C.Backend
   -node_modules
   .
   .
   .

---> Modelo 
-> m.Usuario

### Notas ###

-> Para todo el proyecto se empleará las librerías y archivos JS minificados
-> Usar CamilCase para nombrar las funciones y archivos. Ej: crearUsuario.js
-> Nombrar funciones  y archivos empezando con verbos infinitivos.
-> Los comentarios no deben superar las dos lineas, ser concisos a la hora de explicar la funcionalidad de métodos y variables.